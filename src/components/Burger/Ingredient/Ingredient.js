import React from 'react';
import './Ingredient.css';

const Ingredient = props => {
  switch (props.type) {
    case 'bread-top':
      return (
        <div className="BreadTop">
          <div className="Seeds1" />
          <div className="Seeds2" />
        </div>
        );
    case 'salad':
      return <div className="Salad" />;
    case 'cheese':
      return <div className="Cheese" />;
    case 'bacon':
      return <div className="Bacon" />;
    case 'meat':
      return <div className="Meat" />;
    case 'bread-bottom':
      return <div className="BreadBottom" />;
    default:
      return null;
  }
};

export default Ingredient;